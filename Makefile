PYTHONVER = 3.4
F90FLAGS = "-fopenmp -Ofast -march=native -Wa,-q"

all:	_chemise.so

%.so:	%.f90
	f2py-$(PYTHONVER) -c $< -m $(basename $@) --f90flags=$(F90FLAGS) -lgomp

