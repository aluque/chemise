import numpy as np
import scipy.constants as co
from scipy.interpolate import interp1d
from scipy.integrate import ode

from matplotlib import pyplot as plt

import chemise as ch

TEMPERATURE  = 300
PRESSURE     = 0.1 * co.bar
GAS_DENSITY  = (PRESSURE / (co.k * TEMPERATURE))

class Argon(ch.ReactionSet):    
    def __init__(self):
        super(Argon, self).__init__()

        self.fix({'Ar': GAS_DENSITY})
        
        self.add("e + Ar -> e + Ar*",
                 ch.LogLogInterpolate0("swarm/rate_00.dat"))

        self.add("e + Ar -> e + e + Ar+",
                 ch.LogLogInterpolate0("swarm/rate_01.dat"))

        self.add("Ar+ + e + e -> Ar + e",
                 ch.Constant(1e-38))
        
        self.initialize()

    
def main():
    rs = Argon()
    rs.print_summary()

    n0 = rs.zero_densities(1)

    # Set initial densities
    rs.set_species(n0, 'e', 1 * co.centi**-3)
    rs.set_species(n0, 'Ar+', 1 * co.centi**-3)

    en = np.array([15.0])
    time = np.linspace(0, 1e-3, 1000)

    def f(t, n):
        return rs.fderivs(n[:, np.newaxis], en)

    def jac(t, n):
        return rs.fjacobian(n, en)
    
    r = ode(f)
    r.set_integrator('vode', method='bdf', nsteps=2000,
                     atol=np.full_like(n0, 1e-3),
                     rtol=np.full_like(n0, 1e-8))

    r.set_initial_value(n0, time[0])

    n = rs.zero_densities(len(time))
    n[:, 0] = np.squeeze(n0)


    for i, it in enumerate(time[1:]):
        n[:, i + 1] = np.squeeze(r.integrate(it))

    for s in rs.species:
        plt.plot(time, rs.get_species(n, s), label=s)

    plt.semilogy()
    plt.legend()
    plt.show()
        

if __name__ == '__main__':
    main()
